package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
public class BookUser {
    // 2.1
    @GetMapping(("/api/users/{userId}/books"))
    public String getuserId(@PathVariable int userId){
        return "the book is "+userId;
    }

    // 2.2
    @GetMapping("/api/user/{userId}/books")
    public String GetLowCase(@PathVariable int userid){
        return "the book is "+userid;
    }

    // 2.3
    @GetMapping("/api/segments/good")
    public String CheckReturnLow(){

        return "good";
    }

    @GetMapping("/api/segments/{segmentName}")
    public String CheckReturnLong(@PathVariable String segmentName){

        return "segmentName";
    }

    // 2.4
    @GetMapping("/api/?/" )
    public String UseWildcard(){

        return "success";
    }


    @GetMapping("/api/teacher/?/" )
    public String UseOtherWildcard(){

        return "success";
    }

    //2.5
    @GetMapping("/api/wildcards/*" )
    public String GetWildcards(){

        return "success";
    }

    @GetMapping("/api/*/student/anything" )
    public String GetResult(){

        return "success";
    }

    @GetMapping("/api/wildcard/before/*/after")
    public String JudgeSuccess(){
        return "success";
    }

    @GetMapping("/*/before/teacher/*")
    public String GetStatus(){
        return "success";
    }

    //2.6
    @GetMapping("/api/**/then/after")
    public String GetState(){
        return "success";
    }

    //2.7
    @GetMapping("/api/then/{name:b*}")
    public String UseRegularExpression(@PathVariable String name){

        return "success";
    }
    // 2.8
    @GetMapping("/api/user/then")
    public String GetSuccess(@RequestParam String name,@RequestParam int age){

        return name + "&" + age;
    }
    @GetMapping("/api/first/then")
    public String GetSuccesscode(@RequestParam String name,@RequestParam("class") int klass){

        return name + "&" + klass;
    }
}
