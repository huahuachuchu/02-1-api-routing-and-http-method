package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookUserTest {
    @Autowired
    private MockMvc mockMvc;

    // 2.1
    @Test
    public void should_return_200_and_get_userid() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().is(200))
                .andExpect(content().string("the book is 2"));
    }

    // 2.2
    @Test
    public void should_return_500() throws Exception {
        mockMvc.perform(get("/api/user/3/books"))
                .andExpect(status().is5xxServerError());
    }

    // 2.3
    @Test
    public void should_return_right_code() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().is(200))
                .andExpect(content().string("good"));
    }

    // 2.4
    @Test
    public void should_return_right_status_code() throws Exception {
        mockMvc.perform(get("/api/uuuu" ))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(get("/api/u/"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));
    }

    @Test
    public void should_return_success() throws Exception {
        mockMvc.perform(get("/api/teacher/"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(get("/api/teacher/s/"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));
    }

    // 2.5
    @Test
    public void  should_return_200_and_success() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));
    }

    @Test
    public void  should_return_200() throws Exception {
        mockMvc.perform(get("/api/wildcards/student/anything"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));

    }

    @Test
    public void  should_return_404() throws Exception {
        mockMvc.perform(get("/api/before/after"))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void  should_return_success_and_get_status() throws Exception {
        mockMvc.perform(get("/api/before/teacher/after"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));
    }

    // 2.6
    @Test
    public void  should_return_success_and_get_200() throws Exception {
        mockMvc.perform(get("/api/before/then/after"))
                .andExpect(status().is(200))
                .andExpect(content().string("success"));
    }

    // 2.7
    @Test
    public void  should_return_success_and_get_200_with_egular_expression() throws Exception {
        mockMvc.perform(get("/api/then/bbb"))
                .andExpect(status().isOk())
                .andExpect(content().string("success"));
    }

    //2.8
    @Test
    public void  should_return_success_with_query_string() throws Exception {
        mockMvc.perform(get("/api/user/then?name=xiaoming&age=8"))
                .andExpect(status().isOk())
                .andExpect(content().string("xiaoming&8"));
    }

    @Test
    public void  should_return_result_with_query_string() throws Exception {
        mockMvc.perform(get("/api/user/then?"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void  should_return_result() throws Exception {
        mockMvc.perform(get("/api/first/then?name=xiaoming&class=8"))
                .andExpect(status().isOk())
                .andExpect(content().string("xiaoming&8"));
    }



}